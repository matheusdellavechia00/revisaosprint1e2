package br.com.proway.revisaosprint1e2;

import br.com.proway.revisaosprint1e2.model.Dados;
import br.com.proway.revisaosprint1e2.model.Tv;


import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        int id = 1;
        Dados dados = new Dados();
        while (true) {
            String texto = "               Sistema de tv\n";

            for (int i = 0; i < dados.listatv.size(); i++) {
                texto += dados.listatv.get(i).id + " Modelo: " + dados.listatv.get(i).modelo + ". Polegadas: " + dados.listatv.get(i).polegadas + "\n";
            }

            String[] opcoes = {"Adicionar", "Deletar", "Sair"};

            int opcao = JOptionPane.showOptionDialog(null, texto, "Tv", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if (opcao == 0) {
                String modelo = JOptionPane.showInputDialog("Informe o modelo:");
                int polegadas = Integer.parseInt(JOptionPane.showInputDialog("Informe a(s) polegada(s):"));
                dados.inserirTv(new Tv(id, modelo, polegadas));
                id++;
            } else if (opcao == 1) {
                int idTv = Integer.parseInt(JOptionPane.showInputDialog("Informe o id"));
                dados.deletartv(idTv);
            } else {
                break;


            }
        }
    }
}

