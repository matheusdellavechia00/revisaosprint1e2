package br.com.proway.revisaosprint1e2.model;

public class Tv {
    public int id;
    public String modelo;
    public int polegadas;

    public Tv(int id, String modelo, int polegadas) {
        this.id = id;
        this.modelo = modelo;
        this.polegadas = polegadas;

    }
}